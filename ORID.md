(1) Today I learned tasks, context maps, naming, and git basic.  
&nbsp;&nbsp;&nbsp;1. Task and context map: They divide complex tasks into multiple independent small tasks and draw them into a context map, which can clearly display the tasks.  
&nbsp;&nbsp;&nbsp;2. Naming: For programmers, when writing a function, the first step is to come up with a function name. By using this function name, we can roughly determine what function the function is intended to accomplish.  
&nbsp;&nbsp;&nbsp;3. git basic: With git, we can push our own code to remote warehouses or pull projects from remote warehouses to local warehouses.  
(2) Through today's learning, I am looking forward to applying task decomposition to practical development in future requirements analysis.  
(3) Today's course taught me a lot of knowledge, such as tasking and Naming, which made me clearer and clearer in task division and functional description.  
(4) I hope to have a more detailed understanding of tasks using tasking before writing code in future development.  